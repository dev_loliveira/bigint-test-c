#!/usr/bin/env python

import os, sys, time
import signal
import time
import datetime
from getopt import getopt
from subprocess import *

def arguments_dict(input_array, search_arguments):
    '''
        input_array ['']
        search_arguments ['']
        Exemple:
            input_array = ['--argA', '--argB=1', --argc', '2']
            search_array = ['argA', 'argB=', 'argC=']
    '''
    args = getopt(input_array, '', search_arguments)
    opt_dict = {}
    for i in args[0]:
        option = i[0]
        value = i[1]
        option = option.replace('--', '')
        opt_dict[option] = value
    return (opt_dict)



def intHandler(signal, frame):
    print("Exiting")
    sys.exit(0)

signal.signal(signal.SIGINT, intHandler)

def discover_compiler(src_dir):
    c_ext = ['c', 'cc']
    cpp_ext = ['cpp', 'c++']

    if not os.path.exists(src_dir): return None

    for el in os.listdir(src_dir):
        ext = el.split('.')
        if len(ext) > 1:
            ext = ext[len(ext)-1]
            if ext in c_ext: return "gcc"
            elif ext in cpp_ext: return "g++"
    return None


def discover_source_name(src_dir):
    c_ext = ['c', 'cc']
    cpp_ext = ['cpp', 'c++']

    if not os.path.exists( src_dir ): return None

    for el in os.listdir(src_dir):
        ext = el.split('.')
        if len(ext) > 1:
            ext = ext[len(ext)-1]
            if ext in c_ext: return el
            elif ext in cpp_ext: return el
    return None

args_dict = arguments_dict(sys.argv[1:], [
    'p=', 'project=',
    's=', 'source=',
    'test'
])

root_dir = "./"
if 'p' in args_dict: root_dir = args_dict['p']
elif 'project' in args_dict: root_dir = args_dict['project']

bin_dir = os.path.join(root_dir, "bin")
source_dir = os.path.join(root_dir, "src")
obj_dir = os.path.join(root_dir, "obj")

compiler = discover_compiler(source_dir)
if compiler is None: compiler = "g++"
source_name = discover_source_name(source_dir)
input_file_name = os.path.join(root_dir, "input")
do_test = False if 'test' not in args_dict else True


if 's' in args_dict:
    source_name = args_dict['s']
elif 'source' in args_dict:
    source_name = args_dict['source']

if source_name is None:
    print("No source code found.")
    quit()
elif not os.path.exists( os.path.join(source_dir, source_name) ):
    print("[%s] does not exists."%(os.path.join(source_dir, source_name)))
    quit()


def makeDirectories():
    global bin_dir
    if not os.path.exists(bin_dir):
        print("Creating [%s]"%(bin_dir))
        os.mkdir(bin_dir)
    # if not os.path.exists(obj_dir): os.mkdir(obj_dir)



def build(build_cmd, do_test=False):
    makeDirectories()
    os.system(build_cmd)
    if do_test:
        if os.path.exists("output"):
            expected_output = open("output").read()
            input_file_process = Popen(['cat', input_file_name], stdin=PIPE, stdout=PIPE)
            bin_path = os.path.join(bin_dir, "".join(source_name.split(".")[:len(source_name.split("."))-1]))
            start_time = time.time()
            bin_process = Popen(bin_path, stdin=input_file_process.stdout, stdout=PIPE)
            result = bin_process.stdout.read()
            end_time = time.time()
            if expected_output == result:
                result = "TEST SUCESSFUL"
            else:
                result = "TEST FAILED"
            print("\n>> %s"%(end_time - start_time))
            print("\
*******************************************************\n\
*******************************************************\n\
                        %s                             \n\
*******************************************************\n\
*******************************************************"%(result)
            )
        else:
            print("Cannot test without expected output file.")


build_cmd = "%s %s -o %s" % (compiler,
                     os.path.join(source_dir, source_name),
                     os.path.join(bin_dir, "".join(source_name.split(".")[:len(source_name.split("."))-1]))
                     )
exe_cmd = "%s < %s"%(
    os.path.join(bin_dir, "".join(source_name.split(".")[:len(source_name.split("."))-1])),
    input_file_name,
)

print("Using %s compiler"%(compiler))

build(build_cmd, do_test)

source_last_modification = os.path.getmtime( os.path.join(source_dir, source_name) )

input_last_modification = None
input_actual_time = None
if os.path.exists( input_file_name ):
    input_last_modification = os.path.getmtime( input_file_name )
while True:
    source_actual_time = os.path.getmtime( os.path.join(source_dir, source_name) )
    if input_last_modification:
        input_actual_time = os.path.getmtime( input_file_name )
    if source_last_modification != source_actual_time or \
       input_last_modification != input_actual_time:
        t = datetime.datetime.now()
        print("[%s:%s] [%s] Rebuilding"%(t.hour, t.minute, os.path.join(source_dir, source_name)))
        build(build_cmd, do_test)
        source_last_modification = source_actual_time
        if input_last_modification:
            input_last_modification = input_actual_time
    time.sleep(0.5)
