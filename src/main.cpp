#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <algorithm>

#define MAX_DIGITS 100

using namespace std;

class BigInt {
    private:
        vector<int> _digits;
        
        void _setValue(string n) {
            unsigned long int i;
            _digits.clear();
            for(i=0;i<n.length();i++)
            {
                _digits.push_back( (int)n[i]-48 );
            }
        }
        
        void _setValue(const int n) {
            _setValue(_getStringFrom(n));
        }
        
        string _getStringFrom(const unsigned long int n) {
            char aux[MAX_DIGITS];
            sprintf(aux, "%lu", n);
            return (aux);
        }
        
        string _getStringFrom(vector<int> digits) {
            unsigned long int d;
            string r;
            for(d=0;d<digits.size();d++) r += (char)digits[d]+48;
            return (r);
        }
        
        string _getSum(string op1, string op2) {
            string result = "";
            int addToNext = 0;
            char buf[MAX_DIGITS];
            
            reverse(op1.rbegin(), op1.rend());
            reverse(op2.rbegin(), op2.rend());

            int l1 = op1.length(),
                l2 = op2.length(),
                max = l1 > l2 ? l1 : l2;
            for(int a=0;a<max;a++) {
                int n1, n2, n3;
                if(a < op1.length())
                    n1 = (int)op1[a] - 48;
                else
                    n1 = 0;
                if(a < op2.length())
                    n2 = (int)op2[a] - 48;
                else
                    n2 = 0;
                n3 = n1 + n2;
                if(addToNext > 0)
                    n3 += addToNext;
                addToNext = 0;
                if (n3 < 10) {
                    sprintf(buf, "%d", n3);
                    result += buf;
                }
                else {
                    sprintf(buf, "%d", (n3%10));
                    result += buf;
                    addToNext = n3/10;
                }
            }
            if(addToNext) {
                sprintf(buf, "%d", addToNext);
                result += buf;
            }
            reverse(result.rbegin(), result.rend());
            return result;
        }

    public:
        BigInt() { }
        BigInt(const int n) { _setValue(n); }
        BigInt(string n) { _setValue(n); }
        BigInt(char *n) { _setValue(n); }
        BigInt(const char *n) { _setValue(n); }
        
        string getValue() {
            string r = "";
            for(int i=0;i<_digits.size();i++) {
                r +=(char) _digits[i]+48;
            }
            return r;
        }

        void operator = (string n) {
            _setValue(n);
        }
        // Metodo conveniente para utilizar a classe em conjunto
        // com ostream
        friend ostream& operator << (ostream& o, const BigInt& n) {
            
            unsigned long int i;
            for(i=0;i<n._digits.size();i++) o << n._digits[i];
            return o;
        }

        string operator + (const int n) {
            string op1 = _getStringFrom(_digits),
                    op2 = _getStringFrom(n);
            return (_getSum(op1, op2));
        }

        BigInt& operator += (const char *n)
        {
            string op1 = _getStringFrom(_digits),
                    op2 = n;
            _setValue(_getSum(op1, op2));
            return (*this);
        }
        
        string operator * (const unsigned long int n) {
            string result = "",
                    op1 = "",
                    op2 = "";
            string strN = _getStringFrom(n);
            char buf[MAX_DIGITS];
            int addNext = 0;
            int zeroToAdd = 0;
            for(int i=strN.length()-1;i>=0;i--) {
                result = "";
                int intN = (int)strN[i]-48;
                for(int i2=_digits.size()-1;i2>=0;i2--) {
                    int intDigit = _digits[i2];
                    int pValue = (intDigit * intN) + addNext;
                    addNext = 0;
                    if(pValue > 9) {
                        addNext = (pValue/10);
                        sprintf(buf, "%d", (pValue % 10));
                        result += buf;
                        if(i2-1 < 0) {
                            sprintf(buf, "%d", addNext);
                            result += buf;
                        }
                    }
                    else {
                        sprintf(buf, "%d", pValue);
                        result += buf;
                    }
                }
                
                if(zeroToAdd > 0) {
                    for(int z=0;z<zeroToAdd;z++)
                        result = "0" + result;
                }
                
                reverse(result.rbegin(), result.rend());
                if(op1 == "") op1 = result;
                
                else{
                    op1 = _getSum(op1, result);
                }
                
                zeroToAdd++;
                addNext = 0;
            }
            return op1;
        }
};


int main(int argc, char *argv[]) {
    unsigned long int a,b, i;
    for(a=1;a<argc;a++) {
        BigInt n1 = 1;
        b= atoi(argv[a]);
        for(i=2;i<=b;i++) n1 = n1 * i;
        cout<<n1.getValue().length()<<" digits"<<endl;
    }
}
